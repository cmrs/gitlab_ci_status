# gitlab_ci_status

A Simple Python Script to report the current Continuous Integration 
status for a  gitlab project. The Gitlab Access Token to use, the project id 
and the branches to report can be specified in a config file.

Alternatively, this can also be imported and used as a module.

## CLI Interface

```
gitlab-pipeline-status [--help|-h] [<config>]
    -h, --help: print this help
    <config>:   path to the config file the script shall use
                if not supplied, tries to use config.json
                and then test_config.json from the script's directory
```

## Module Interface
```
import gitlab_ci_status as gcis
try:
    conf = gcis.load_config_file("gitlab_ci_config.json")
    branches = gcis.get_pipeline_status_from_config(conf)
    # or alternatively:
    # branches = gcis.get_pipeline_status(
    #    "GITLAB_ACCESS_TOKEN", "PROJECT_ID", ["master", "some-other-branch"]
    # )
    for br, status in branches.items():
        print(br + ": " + status)
        
except gcis.GitlabCiStatusError as e:
    sys.stderr.write(str(e) + "\n")
    exit(-1)

```
