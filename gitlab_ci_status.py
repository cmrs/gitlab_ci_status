#!/usr/bin/env python3
import requests
import json
import sys
import os
from dateutil import parser
from datetime import datetime, timezone

import json

class Config:
    def __init__(self, access_token, project_id, branches):
        self.access_token = access_token
        self.project_id = project_id
        self.branches = branches

class GitlabCiStatusError(Exception):
    pass

def load_config_file(filename):
    cfg = _try_load_config_file(filename)
    if cfg is None: 
        raise GitlabCiStatusError(
            "Failed to load config from '" + filename + "'"
        )
    return cfg

def _try_load_config_file(filename):
    try:
        with open(filename) as json_data_file:
            data = json.load(json_data_file)

        access_token = str(data["gitlab_access_token"])
        project_id = str(data["project_id"])
        branches = data["branches_to_report"]
        for i in range(0, len(branches)):
            branches[i] = str(branches[i])
        return Config(access_token, project_id, branches)
    except (json.decoder.JSONDecodeError, TypeError) as e: 
        raise GitlabCiStatusError("Failed to parse " + filename + ": " + str(e))
    except IOError:
        return None


def _parse_args():
    argc = len(sys.argv)
    if argc > 1:
        if(argc > 2 or sys.argv[1] == "-h" or sys.argv[1] == '--help'):
            print("""
gitlab-pipeline-status [--help|-h] [<config>]
    -h, --help: print this help
    <config>:   path to the config file the script shall use
                if not supplied, tries to use config.json
                and then test_config.json from the script's directory
            """.strip())
            exit(-1 if argc > 2 else 0)
        conf = load_config_file(sys.argv[1])
        return conf

    script_dir = sys.path[0]
    os.chdir(script_dir)
    fileopts = ["config.json", "test_config.json"]
    for f in fileopts:
        conf = _try_load_config_file(f)
        if conf is not None: return conf
    else:
        raise GitlabCiStatusError(
            "Failed to find config, candidates: " + str(fileopts)
        )

def gitlab_api_request(url, access_token, attributes={}):
    headers = {
            'content-type': 'application/json',
            'Accept-Charset': 'UTF-8',
            'PRIVATE-TOKEN': access_token
    }
    r = requests.get(
        "https://gitlab.com/api/v4/" + url,
        headers=headers,
        params=attributes
    )
    if (int(r.status_code) / 100) != 2:
        raise GitlabCiStatusError(
            "Gitlab answered with error:\n" + r.text
        )

    return json.loads(r.text)


def get_most_recent_pipeline_status(pipelines):
    recent = None
    recent_date = parser.parse("1990-01-01T00:00:00Z")
    for pl in pipelines:
        date = parser.parse(pl["created_at"])
        if(date > recent_date):
            recent = pl
            recent_date = date
    if recent == None: return "unknown"
    return recent["status"]

def get_pipelines(access_token, project_id):
    return gitlab_api_request(
        "projects/" + project_id + "/pipelines",
        access_token
    )

def get_pipeline_status_from_config(config):
    return get_pipeline_status(
        config.access_token, config.project_id, config.branches
    )

def get_pipeline_status(access_token, project_id, branches):
    pipelines = get_pipelines(access_token, project_id)
    branch_pls = {}
    for pl in pipelines:
        br = pl["ref"]
        if br in branch_pls:
            branch_pls[br].append(pl)
        else:
            branch_pls[br] = [pl]

    branch_stats = {}
    for br in branches:
        if br in branch_pls:
            branch_stats[br] = get_most_recent_pipeline_status(branch_pls[br])
        else:
            branch_stats[br] = "no data"
    
    return branch_stats

if __name__== "__main__":
    try:
        conf = _parse_args()
        if conf is None: exit(0)
        res = get_pipeline_status(
            conf.access_token, conf.project_id,  conf.branches
        )
        for br, status in res.items():
            print(br + ": " + status)
    except GitlabCiStatusError as e:
        sys.stderr.write(str(e) + "\n")
        exit(-1)
